import { Todo } from './todo/model/todo.model';
import { ActionReducerMap } from '@ngrx/store';

import { todoReducer } from './todo/Reducer/todo.reducer';
import { FiltreReducer } from './filter/Reducer/filter.reducer';
import { filtresValides } from './filter/actions/filter.actions';

// Le todo prop est l'etat passe dans le App module.
// Liste de TOUS les reducers
// Toute les conf des reducer qui est dans le Store Module.
export interface AppState {
    todos: Todo[]; // reducer function qui return un tableau de todo
    filtres: filtresValides; // reducer function qui return un string (All/Active/Completed)
}

export const TousMesReducers: ActionReducerMap<AppState> = {
    todos: todoReducer,
    filtres: FiltreReducer
};



