import { filtresValides, ActionsType, SET_FILTER } from '../actions/filter.actions';

const etatInitial: filtresValides = 'All';

// Ce reducer va juste envoyer All/Active/Completed == de filtresValides
export function FiltreReducer(state = etatInitial, action: ActionsType): filtresValides {
    switch (action.type) {
        case SET_FILTER:
            return action.filtre;

        default:
            return state;
    }
}
