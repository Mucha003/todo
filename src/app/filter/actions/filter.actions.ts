import { Action } from '@ngrx/store';

export type filtresValides = 'All' | 'Active' | 'Completed';

export const SET_FILTER = '[Filter] set Filter';

export class SetFilterAction implements Action {
    readonly type = SET_FILTER;

    // filtre Menu en bas des todos All/Active/Completed
    constructor(public filtre: filtresValides) {
    }
}

// Type de texte vlide pour le reducer.
export type ActionsType = SetFilterAction;
