import { Pipe, PipeTransform } from '@angular/core';
import { Todo } from '../../todo/model/todo.model';
import { filtresValides } from '../actions/filter.actions';

@Pipe({
  name: 'filterTodo'
})
export class FilterPipe implements PipeTransform {

  transform(tasks: Todo[], filtreAApliquer: filtresValides): Todo[] {

    // console.log(tasks);
    // console.log(filtreAApliquer);

    switch (filtreAApliquer) {
      case 'Completed':
        return tasks.filter(x => x.isCompleted);

      case 'Active':
        return tasks.filter(x => !x.isCompleted);

      default:
        return tasks;
    }
  }

}
