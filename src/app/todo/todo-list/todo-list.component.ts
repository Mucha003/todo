import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducers';
import { Todo } from '../model/todo.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styles: []
})
export class TodoListComponent implements OnInit {

  todoslist: Todo[] = [];
  filterName: string;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.getListTodos();
  }

  getListTodos() {
    this.store.subscribe(x => {
      this.todoslist = x.todos;
      this.filterName = x.filtres;
    });
  }

}
