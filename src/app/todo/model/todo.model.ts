
export class Todo {
    public id: number;
    public texte: string;
    public isCompleted: boolean;

    constructor(texte: string) {
        this.id = Math.random();
        this.texte = texte.charAt(0).toUpperCase() + texte.slice(1);
        this.isCompleted = false;
    }
}
