import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducers';
import * as todoActions from '../actions/todo.actions';
import { AddTodoAcion } from '../actions/todo.actions';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styles: []
})
export class TodoAddComponent implements OnInit {

  txtInput: FormControl;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.txtInput = new FormControl('', Validators.required);
  }

  AddTodos() {
    if (!this.txtInput.valid) {
      return;
    }
    const action = new todoActions.AddTodoAcion(this.txtInput.value);
    this.store.dispatch(action);

    // reset input
    this.txtInput.setValue('');
  }

}
