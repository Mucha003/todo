import * as fromTodo from '../actions/todo.actions';
import { Todo } from '../model/todo.model';
import { Action } from '@ngrx/store';

// init avec une etat
const todo1 = new Todo('Guitar');
const todo2 = new Todo('Redux');
const todo3 = new Todo('Net Core');

todo2.isCompleted = true;

const etatInitial: Todo[] = [todo1, todo2, todo3];

// On fait pas push car on pourra pas suivre les changement des actions
// /!\ on doit toujour renvoyer un new Array(qui sert d'etat). pour empecher la ref de cet etat.
export function todoReducer(state = etatInitial, action: fromTodo.ActionsType): Todo[] {
    switch (action.type) {
        case fromTodo.ADD_TODO:
            const todo = new Todo(action.text);
            // on clone l'etat actuel.
            return [...state, todo]; // creation d'un new array [...state]

        case fromTodo.EDIT_TODO:
            return state.map(x => {
                if (x.id === action.id) {
                    return {
                        ...x, // copie moi tout l'obj
                        texte: action.titre // modifie moi ceci
                    };
                } else {
                    return x;
                }
            });

        case fromTodo.DELETE_TODO:
            // filter va returner un new array avec une cndition
            // return tous les element qui ne pas cet Id
            return state.filter(x => x.id !== action.id);

        case fromTodo.DELETE_ALL_TODO:
            return state.filter(x => !x.isCompleted);

        case fromTodo.TOGGLE_TODO:
            // Comme un foreach (map cree un new array)
            return state.map(x => {
                if (x.id === action.id) {
                    // Nouveau Todo Car les items sont ici toujour en ref
                    // donc on pourra pas suivre son etat.
                    return {
                        ...x, // copie les values par defaut qui avait l'objet (Update Method)
                        isCompleted: !x.isCompleted // les prop que j'ajoute ici va les changer.
                    };
                } else {
                    return x;
                }
            });

        case fromTodo.TOGGLE_ALL_TODO:
            return state.map(x => {
                return {
                    ...x,
                    isCompleted: action.completedTask,
                };
            });

        default:
            return state;
    }
}
