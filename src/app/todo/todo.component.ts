import { Component, OnInit } from '@angular/core';
import { ToggleAllTodoAcion } from './actions/todo.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducers';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styles: []
})
export class TodoComponent implements OnInit {

  completedTask = false; // Marquer tout les task a Fait.

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
  }

  toggleAll() {
    this.completedTask = !this.completedTask;
    const action = new ToggleAllTodoAcion(this.completedTask);
    this.store.dispatch(action);
  }


}
