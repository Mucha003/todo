import { Action } from '@ngrx/store';

export const ADD_TODO = '[TODO] Ajouter Todo';
export const EDIT_TODO = '[TODO] Edit Todo';
export const DELETE_TODO = '[TODO] Delete Todo';
export const DELETE_ALL_TODO = '[TODO] Delete All Todo';

export const TOGGLE_TODO = '[TODO] Toggle Todo'; //  changer l'etat du todo(completed or not)
export const TOGGLE_ALL_TODO = '[TODO] Toggle all Todo'; //  changer l'etat de TOUS les todo(completed or not)

export class AddTodoAcion implements Action {
    readonly type = ADD_TODO;

    constructor(public text: string) {
    }
}

export class EditTodoAcion implements Action {
    readonly type = EDIT_TODO;

    // Kel est l'id du todo que je dois changer.
    constructor(public id: number, public titre: string) {
    }
}

export class DeleteTodoAcion implements Action {
    readonly type = DELETE_TODO;

    // Kel est l'id du todo que je dois changer.
    constructor(public id: number) {
    }
}

export class DeleteALLTodoAcion implements Action {
    readonly type = DELETE_ALL_TODO;
}

export class ToggleTodoAcion implements Action {
    readonly type = TOGGLE_TODO;

    // Kel est l'id du todo que je dois changer.
    constructor(public id: number) {
    }
}


export class ToggleAllTodoAcion implements Action {
    readonly type = TOGGLE_ALL_TODO;

    constructor(public completedTask: boolean) {
    }
}




// Type de texte vlide pour le reducer.
export type ActionsType = AddTodoAcion |
                          ToggleTodoAcion |
                          EditTodoAcion |
                          DeleteTodoAcion |
                          ToggleAllTodoAcion |
                          DeleteALLTodoAcion;
