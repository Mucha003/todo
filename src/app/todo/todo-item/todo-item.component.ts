import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Todo } from '../model/todo.model';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducers';
import { ToggleTodoAcion, EditTodoAcion, DeleteTodoAcion } from '../actions/todo.actions';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styles: []
})
export class TodoItemComponent implements OnInit {

  @Input() todoItemChild: Todo;
  // ViewChild == comme le Jquery
  @ViewChild('idTxtInputEdit') idTxtInputEdit: ElementRef;

  checkField: FormControl;
  txtInput: FormControl;

  Editer: boolean;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.InitForm();
    this.checkField.valueChanges.subscribe(() => {
      // console.log(x))
      const action = new ToggleTodoAcion(this.todoItemChild.id);
      this.store.dispatch(action);
    });

    // console.log(this.todoItemChild);
  }

  InitForm() {
    this.checkField = new FormControl(this.todoItemChild.isCompleted);
    this.txtInput = new FormControl(this.todoItemChild.texte, Validators.required);
  }

  Edit() {
    this.Editer = true;

    setTimeout(() => {
      // Selectionne tout le text Du double Click
      this.idTxtInputEdit.nativeElement.select();
    }, 1);
  }

  // Pour enlever le Input
  EditionTermine() {
    this.Editer = false;

    if (this.txtInput.invalid) {
      return;
    }

    if (this.txtInput.value === this.todoItemChild.texte) {
      return;
    }

    const action = new EditTodoAcion(this.todoItemChild.id, this.txtInput.value);
    this.store.dispatch(action);
  }

  RemoveItem() {
    const action = new DeleteTodoAcion(this.todoItemChild.id);
    this.store.dispatch(action);
  }

}
