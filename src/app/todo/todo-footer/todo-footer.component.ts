import { Component, OnInit } from '@angular/core';
import { filtresValides, SetFilterAction } from '../../filter/actions/filter.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducers';
import { Todo } from '../model/todo.model';
import { DeleteALLTodoAcion } from '../actions/todo.actions';


@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styles: []
})
export class TodoFooterComponent implements OnInit {

  FiltreList: filtresValides[] = ['All', 'Active', 'Completed'];
  filterNow: filtresValides;
  taskActive: number;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.subscribe(x => {
      this.CountTaskActive(x.todos);
      this.filterNow = x.filtres;
    });
  }

  changeFlter(filtreItem: filtresValides) {
    const action = new SetFilterAction(filtreItem);
    this.store.dispatch(action);
  }

  CountTaskActive(todos: Todo[]) {
    this.taskActive = todos.filter(x => !x.isCompleted).length;
  }

  clearAllCompleted() {
    const action = new DeleteALLTodoAcion();
    this.store.dispatch(action);
  }
}
